import { Meteor } from 'meteor/meteor';
import { EJSON } from 'meteor/ejson'
import '/imports/api/scans.js';
import '/imports/api/touchScreenScans.js';
import { WallUsers } from '/imports/api/wallUsers';
import { ConfigValues } from '/imports/api/configValues';
import { TouchEvents } from '/imports/api/touchEvents';

import bodyParser from 'body-parser';
import { Picker } from 'meteor/meteorhacks:picker';

Meteor.startup(() => {
  // code to run on server at startup
  /*
  if(WallUsers.find().count() === 0){
    let u1 = { firstName: 'Steel', lastName: 'Jones', classification: 'Intern', yearStartedWork: '0.1', city: 'New York', state: 'NY', btLocalName: 'LeoSF_00537', lat: 40.71, lng: -74.0 };
    let u2 = { firstName: 'Stephen', lastName: 'Peek', classification: 'CTO', yearStartedWork: '7', city: 'Buffalo', state: 'NY', btLocalName: 'LeoSF_00542',lat: 42.89, lng: -78.88 };
    WallUsers.insert( u1 );
    WallUsers.insert( u2 );
  }
  */
  Picker.middleware(bodyParser.urlencoded({ extended: false }));
  Picker.middleware(bodyParser.json());

  var post = Picker.filter(function(req, res) {
    return req.method == "POST";
  });

  post.route('/status/:readerId', function(params, req, res, next) {
    let body = req.body;
    console.log('Total Tags Found', body.length)
    let readerId = params.readerId
    // let scan = body[0]
    // let scan = body[Math.floor(Math.random() * body.length)]
    let scans = _.sortBy(body, 'rssi');
    // scan = _.first(scans, 10)
    // let codes = _.pluck(scans.slice(0, 10), 'code');
    let scan;
    // if (scan && body.length > 0) {
    if (scans && scans.length > 0) {
      scan = {}
      scan.readerId = readerId
      let uuids = _.pluck(scans.slice(0, 10), 'ibeaconUuid');
      // console.log('scan', scan)
      if (scan && scan.readerId && uuids.length > 0) {
        let codes = []
        uuids.map( a => {
          let code = a.substr(a.length - 5)
          codes.push(code)
        });
        scan.codes = codes
        console.log('Scan', scan)
        // console.log('Code:', code)
        Meteor.call( 'addScan', scan, function(err,response){
          if(err) console.log(err);
        });
      }
    }
    res.end();
  });
});
/*
Meteor.server.stream_server.server.addListener('connection', function (socket) {
  var old = socket._events.data
  socket._events.data = function () {
    let data = EJSON.parse(arguments[0]);
    console.log(data);
    old.apply(this, arguments)
  }
})
*/
Meteor.methods({
  isBadgeRegistered( badgeName ){
    console.log('******* ', badgeName );
    let users = WallUsers.find({ "epcs.badgeName": badgeName}).fetch();
    if(users.length == 0){
      return false;
    }else{
      return users;
    }
  }
});
