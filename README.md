### Options
Day 1 (Live Check-ins): http://localhost:3000?1=1
Day 2 (Touch Screen Map): http://localhost:3000

*Session* can be set with the _session_ parameter
*Event ID* can be set with the _event-id_ parameter

Ex: _http://localhost:3000/?1=1&session=2&event-id=571_

#### To-DO
Build in more of these features into Eventfinity so that session, test event ID, event location, logo, background, etc. can be set from the Admin portal. Until then, add more functionality that can be turned on/off through URL parameters. 