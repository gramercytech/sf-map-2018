import { Meteor } from 'meteor/meteor';
import { ReactiveVar } from 'meteor/reactive-var'

import './btBadge.html';
import './main.css';

Template.btBadge.onCreated(function(){
  this.user = new ReactiveVar('');
  window.user = this.user;
});

Template.btBadge.onRendered( () => {
});

Template.btBadge.helpers({
  hasUser(){
    return Boolean(Template.instance().user.get());
  },
  userName(){
    let user = Template.instance().user.get();
    return user;
  }
});
Template.btBadge.events({
  'click #read-badges': (event, template) => {
    navigator.bluetooth.requestDevice(
      /*
      {
        acceptAllDevices:true

      }
      */
      {
        filters: [{ namePrefix: "LeoSF" }]
      }
    ).then( device => {
      let { name } = device;
      Meteor.call('isBadgeRegistered', name, (err,result) => {
        if(err){
          console.log('Error: ', err, result);
        }else if(result){
          console.log( result.map( u => u.firstName + ' ' + u.lastName) );
        }else{
          console.log("badge not registered");
        }
      });
    });
  },
  'click #user-container': (event, template) => {
    
    navigator.bluetooth.requestDevice(
      {
        acceptAllDevices:true
      }
    ).then( device => {
      let { name } = device;
      let userExists = WallUsers.findOne({"epcs.badgeName": name})
      if(!userExists){
        template.user.set('');
      }else{
        template.user.set( userExists.firstName + ' ' + userExists.lastName );
      }
    });
  },    
});

