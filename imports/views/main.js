'use strict';

import 'animate.css/animate.css';
import './main.html';
import './main.css';

import  THREE  from 'three';
import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Meteor } from 'meteor/meteor';
import moment from 'moment';
// import mqtt from  'mqtt';
// import { mqtt } from 'meteor/mkarliner:mqtt';
const OrbitControls = require("three-orbit-controls")(THREE);
//const request = require('request');
import { drawThreeGeo } from '../lib/threeGeoJSON'
import { nodeImageFragmentShader, nodeImageVertexShader } from '../lib/shaders.js'
import { postalToState } from '../lib/statePostalCodes';
import { cities } from '../lib/cities';
// import { stateInfo } from '../lib/sessionOne';
// import { stateInfo } from '../lib/sessionTwo';
import { stateInfo } from '../lib/sessionThree';
// import { stateInfo } from '../lib/stateInfoTest';
import { Scans } from '/imports/api/scans'
import { WallUsers } from '/imports/api/wallUsers'
import { ConfigValues } from '/imports/api/configValues'
import { TouchEvents } from '/imports/api/touchEvents'

//This is a server in memory collection that does not persist in mongo
const TouchScreenScans = new Mongo.Collection('touchScreenScans');

const TWEEN = require('tween.js');

Array.prototype.inArray = function(comparer) { 
  for(var i=0; i < this.length; i++) { 
    if(comparer(this[i])) return true; 
  }
  return false; 
}; 

Array.prototype.pushIfNotExist = function(element, comparer) { 
  if (!this.inArray(comparer)) {
    this.push(element);
  }
}; 

let app = {
  initScene(){
    let ambientLight;
    this.mode = 'day1';
    this.activeUser = false;
    this.textOverlayParent = document.getElementById( 'text-overlays' );
    this.timeElement = document.querySelector('.time');
    this.sessionElement = document.querySelector('.session-text');
    this.temperatureElement = document.querySelector('.temperature');
    this.checkinsElement = document.querySelector('.check-in-count');
    this.attendeesList = document.querySelector('.attendees-list');
    this.attendeesListContainer = document.querySelector('.attendees-list-container');
    this.activeContainer = document.querySelector('.active-container');
    this.hasScanElement = document.querySelector('.day-2 .has-scan');
    this.scanName = document.querySelector('.day-2 .has-scan .scan-name');
    this.activeStateName = document.querySelector('.active-container .state-name');
    this.selectedAttendeeEl = document.querySelector('.active-container .selected-attendee');
    this.akhiContainer = document.querySelector('.ak-hi-container');
    this.smallStateLabelsContainer = document.querySelector('.small-state-labels');
    this.resetButton = document.querySelector('.day-2 .reset-btn')
    this.touchIcons = new Array(...document.querySelectorAll('.touch-icon'));
    // this.numberFontSize = 200;
    // Can adjust by state if this is determined to be too small
    this.numberFontSize = 140;
    this.testStates = ['Florida', 'Texas', 'Texas', 'Texas', 'Texas', 'Texas', 'North Carolina', 'Nevada', 'Florida', 'Florida' ];
    this.pins = [];
    this.queue = [];
    this.pastQueue = [];
    this.recentTags = [];
    this.checkInMeshes = [];
    this.nextOverlayIndex = 0;
    this.nextListItemIndex = 0;
    this.easing = TWEEN.Easing.Exponential.Out;
    this.reverseEasing = TWEEN.Easing.Exponential.Out;
    this.defaultCameraDistance = 150;
    // this.highlightStateColor = 154 / 255;
    // this.highlightStateColor = "0xDD2A26";
    // this.highlightStateColor = "rgb(221, 42, 38)"
    // this.highlightStateColor = "rgb(60, 60, 60)";
    this.highlightStateColor = "rgb(74, 74, 74)";
    this.tetherDistance = 8;
    this.maxListItems = 10;
    this.mapSize = 250;
    this.nodeSize = 5;
    // this.pinSize = 3;
    this.pinSize = 2.5;
    this.smallScale = 10e-6;
    this.resizeScales = {
      Hawaii: 1.4,
      Alaska: 0.3
    }
    this.offsetPositions = {
      Alaska: new THREE.Vector3( 52, -51 - 68, 0),
      //Alaska: new THREE.Vector3( 52, -51, 0),
      Hawaii: new THREE.Vector3( 67, 6, 0 ),
    }
    this.customNodeDistances = {
      // 'California': 10.25,
      'Minnesota': 6,
      'Montana': 5,
      'Washington': 5,
    }
    this.offsetCenters = {
      Alaska: new THREE.Vector3( 8, 8, 0 ),
      Arkansas: new THREE.Vector3( -0.5, 0, 0 ),
      California: new THREE.Vector3( -1, 0, 0 ),
      Connecticut: new THREE.Vector3( 0.5, 1, 0 ),
      Delaware: new THREE.Vector3( -1, -1, 0 ),
      Florida: new THREE.Vector3( 3, 0, 0 ),
      Hawaii: new THREE.Vector3( 3, -1.5, 0 ),
      Idaho: new THREE.Vector3( -1, -2, 0 ),
      Illinois: new THREE.Vector3( 0.5, 0.5, 0 ),
      Indiana: new THREE.Vector3( 0.25, 0, 0 ),
      Kentucky: new THREE.Vector3( 1, -0.5, 0 ),
      Louisiana: new THREE.Vector3( -1.5, 0, 0 ),
      Maryland: new THREE.Vector3( 1, 1.5, 0 ),
      Michigan: new THREE.Vector3( 2.5, -2, 0 ),
      Minnesota: new THREE.Vector3( -1.5, 0, 0 ),
      Montana: new THREE.Vector3( 1, 0.5, 0 ),
      'New Jersey': new THREE.Vector3( 1, 0, 0 ),
      'New York': new THREE.Vector3( 1, 0.25, 0 ),
      Nevada: new THREE.Vector3( 0, 1, 0 ),
      'North Carolina': new THREE.Vector3( 1.5, 0, 0 ),
      Oklahoma: new THREE.Vector3( 1.5, 0, 0 ),
      'Rhode Island': new THREE.Vector3( -1, 1, 0 ),
      'South Dakota': new THREE.Vector3( 0, 0.25, 0 ),
      'West Virginia': new THREE.Vector3( -1, -0.5, 0 ),
      Texas: new THREE.Vector3( 2, 0, 0 ),
      Vermont: new THREE.Vector3( -1.5, 0, 0 ),
      Virginia: new THREE.Vector3( 1.5, -0.5, 0 ),
      Wisconsin: new THREE.Vector3( 0, -0.5, 0 ),
    }
    this.postalToState = postalToState;
    this.cities = cities;
    this.stateInfo = stateInfo;
    this.sproutDuration = 1000;
    this.reverseSproutDuration = 500;
    this.idleTimeoutDuration = 1000 * 60;
    this.noScanTimeoutDuration = 1000 * 10;
    this.textureLoader = new THREE.TextureLoader(),
    this.scene =  new THREE.Scene(),
    this.canvas = document.getElementById( 'map-canvas' );
    this.renderer = new THREE.WebGLRenderer( { canvas: this.canvas, alpha: true, antialias: true });
    this.renderer.setPixelRatio( window.devicePixelRatio );
    this.renderer.setSize( window.innerWidth, window.innerHeight );
    this.renderer.setClearColor( 0x000000, 0.0 );
    this.renderer.setClearAlpha(0.0);

/*
    this.camera = new THREE.PerspectiveCamera( 35, window.innerWidth / window.innerHeight, 1, 10000 );
*/
    let cw = this.canvas.offsetWidth;
    let ch = this.canvas.offsetHeight;
    this.camera = new THREE.OrthographicCamera( cw * -1/2,
                                           cw * 1/2,
                                           ch * 1/2,
                                           ch * -1/2,
                                           -500, 1000 );
    // this.camera.zoom = 17;
    this.camera.zoom = 15;
    // this.camera.zoom = 10;

    this.camera.updateProjectionMatrix();
    this.raycaster =  new THREE.Raycaster();
    this.mouse = { x: 0, y: 0 };
    this.camera.position.set( 0, 0, this.defaultCameraDistance );
    this.camera.lookAt( this.scene );

    ambientLight = new THREE.AmbientLight(0xffffff);
    this.scene.add( ambientLight );

    this.directionalLight = new THREE.DirectionalLight( 0xffffff, 1.0 );
    this.directionalLight.position.set( 1, 1, 1);

    this.scene.add( this.directionalLight );

    this.statesGroup = new THREE.Object3D();
    this.scene.add( this.statesGroup );
    this.states = [];
    this.borders = [];

    if(window.location.search.indexOf('controls') !== -1){

      this.cameraControls = new OrbitControls( this.camera, this.renderer.domElement);
      this.cameraControls.target.set( 0, 0, 0 );
      this.cameraControls.mouseButtons.PAN = 0;
      this.cameraControls.mouseButtons.ORBIT = 2;
    }else{
      this.skipControls = true;
      this.cameraControls = new OrbitControls( this.camera, this.renderer.domElement);
      this.cameraControls.target.set( 0, 0, 0 );
      this.cameraControls.enableRotate = false;
      this.cameraControls.enablePan = false;
      this.cameraControls.enableZoom = false;
    }
  },
  convertToPlaneCoords(coordinates, radius) {
    var lon = coordinates[0];
    var lat = coordinates[1];

    return [(lat / 180) * radius, (lon / 180) * radius];
  },
  getCityData( stateName ) {
    return this.cities[stateName.trim()]
  },
  userLatLngToSceneMapCoordinates( userObject ){
    let pos, stateName, cityData;
    cityData = this.getCityData(userObject.what_is_office_state)  
    // stateName = this.getStateNameFromPostal( userObject.what_is_office_state );
    stateName = userObject.what_is_office_state.replace(/['"]+/g, '').trim()
    let stateObject = this.scene.getObjectByName( stateName );
    if( userObject.lat && userObject.lng ){
      let planeCoords = this.convertToPlaneCoords( [userObject.lat, userObject.lng], this.mapSize );
      pos = new THREE.Vector3( ...planeCoords, 1 );

      //If this is in AK or HI offset the position
      if(Object.keys(this.offsetPositions).includes( stateName )){
        if(stateName === 'Alaska'){
          pos.y *= 1.8;
        }
        let scale = this.resizeScales[stateName];
        pos.add( this.offsetPositions[stateName] );
        let offset = pos.clone().sub( stateObject.position ).multiplyScalar( scale );
        pos = stateObject.position.clone().add(offset);
        pos.z = 1;
        //pos.y += this.pinSize / 2;// * scale;
      }
    // } else if (cityData && cityData.lat && cityData.lng) {
    } else if (false) {
      let planeCoords = this.convertToPlaneCoords( [cityData.lat, cityData.lng], this.mapSize );
      pos = new THREE.Vector3( ...planeCoords, 1 );

      //If this is in AK or HI offset the position
      if(Object.keys(this.offsetPositions).includes( stateName )){
        if(stateName === 'Alaska'){
          pos.y *= 1.8;
        }
        let scale = this.resizeScales[stateName];
        pos.add( this.offsetPositions[stateName] );
        let offset = pos.clone().sub( stateObject.position ).multiplyScalar( scale );
        pos = stateObject.position.clone().add(offset);
        pos.z = 1;
        //pos.y += this.pinSize / 2;// * scale;
      }
    } else if(stateObject){
      pos = stateObject.getWorldPosition();
      pos.z = 1;
      // pos.y += this.pinSize / 2;
    }else{
      pos = false;
    }
    return pos
  },
  initMap( mapData ){

    let parsedData = JSON.parse( mapData );
    /*
    parsedData.features = parsedData.features.filter( f => {
      return f.properties.iso_a2 === "US";
    });
    */
    parsedData.features = parsedData.features.filter( f => {
      return f.properties.NAME !== "Puerto Rico"
    });    
    drawThreeGeo( this.scene, parsedData, this.mapSize, 'plane', {} );
    if(this.mode == 'day1'){
      this.camera.position.x = -145;
    }else{
      this.camera.position.x = -152;
    }

    this.camera.position.y = 52;
    //if(this.cameraControls){
      this.cameraControls.target.set(this.camera.position.x, this.camera.position.y, 0);
    //}
    this.scene.children.filter(c => c instanceof THREE.Line).forEach( (c,i) => {
      if( c.stateName === 'Alaska' ) {
        c.geometry.scale(1, 1.8, 1)
      }
      this.addStateShape( c );
      this.scene.remove( c );
      c.geometry.dispose();
    });

    this.statesGroup.children.forEach( c => {
      //c.postal = parsedData.features.find( d => d.properties.name === c.name ).properties.postal;
      //this.postalToState[c.postal] = c.name;
      try{
        c.postal = Object.keys(this.postalToState).map( k => [k, this.postalToState[k]] ).find( a => a[1] === c.name )[0];
      }catch(e){
        console.log( "Couldn't get postal code for state ", c.name );
      }
      this.centerState(c) 
      this.createStateChildNodes( c );
    });
    this.resizeStates();
  },
  loadMap(){
    let loader = new THREE.FileLoader();
    //loader.load('countries_states.json', this.initMap.bind(this));
    loader.load('states20m.json', this.initMap.bind(this));
  },
  animate(){
    requestAnimationFrame(this.animate.bind(this));
    this.updateScene();
    this.renderer.render( this.scene, this.camera );
  },
  updateScene(){
    if( this.statesGroup ){
      this.statesGroup.children.forEach( s => {
        s.childNodes.children.forEach( cn => {
          cn.node.material.uniforms.u_time.value += 0.01;
        });
      });
    }
    TWEEN.update();
  },
  addStateShape( state ){
    let stateGroup;
    state.geometry.computeBoundingBox();
    let shape = new THREE.Shape( state.geometry.vertices );
    let geo = new THREE.ShapeBufferGeometry( shape );
    // let material = new THREE.MeshBasicMaterial({ side: THREE.FrontSide, color: 0x6E6E6E });
    let material = new THREE.MeshBasicMaterial({ side: THREE.FrontSide, color: 0x9B9B9B });
    let mesh = new THREE.Mesh( geo, material );
    let instanceGroup = new THREE.Object3D();
    instanceGroup.add( mesh );
    instanceGroup.mesh = mesh;
    //instanceGroup.position.sub( center );

    let stateExists = this.scene.getObjectByName( state.stateName );
    
    if( stateExists ){
      stateGroup = stateExists;
    }else{
      stateGroup = new THREE.Object3D();
      if( state.stateName ) stateGroup.name = state.stateName;
      this.statesGroup.add( stateGroup );
    }
    stateGroup.add( instanceGroup );
    this.states.push( mesh );
    let edges = new THREE.EdgesGeometry( geo );
    let line = new THREE.LineSegments( edges, new THREE.LineBasicMaterial( { color: 0xffffff } ) );
    instanceGroup.add( line );
    instanceGroup.line = line;
    this.borders.push( line );
  },

  handleCanvasClick(event){
    if( this.mode === 'day1' ) return;
    let coordinates = this.mouseClickToScene(event);
    this.mouse.x = coordinates[0] / window.innerWidth * 2;
    this.mouse.y = coordinates[1] / window.innerHeight * 2;
    this.raycaster.setFromCamera(this.mouse, this.camera);
    if( this.activeState ){
      let nodes = this.activeState.parent.parent.childNodes.children.map( c => c.node );
      let intersectsNode = this.raycaster.intersectObjects( nodes );
      if( intersectsNode.length > 0 ){
        var userId = parseInt(intersectsNode[0].object.userId)
        for (var i=0; i < app.selectedAttendees.length; i++) {
          if (app.selectedAttendees[i].event_attendee_id === userId ) {
            app.showSelectedAttendee( app.selectedAttendees[i] );
            // app.unhighlightNodes(nodes)
            // app.highlightNode(intersectsNode[0].object)
          }
        }
        return;
      }
    }
    let intersectsState = this.raycaster.intersectObjects( this.states );
    if( intersectsState.length > 0 ){
      this.handleStateClick( intersectsState[0].object );
    }else{
      this.handleActiveUser();      
    }
  },
  highlightNode(node) {
    node.material.uniforms.tint.value = 1.0;
    node.material.uniforms.u_time.value = 0.0;
  },
  unhighlightNode(node){
    node.material.uniforms.tint.value = 0.0;
  },
  unhighlightNodes(nodes){
    nodes.map( a => {
      this.unhighlightNode( a );
    });
  },
  handleStateClick( stateObj ){
    if( !this.activeUser ){
      this.handleActiveUser();
    }
    if( stateObj.parent.parent.isHighlighed ){
      //this.unhighlightState( stateObj );
      this.unhighlightState( stateObj, this.activeState !== undefined );
      // this.reset();
    }else if(this.activeState !== undefined){
      this.unhighlightState( this.activeState );
      this.highlightState( stateObj, true );
    }else{
      this.highlightState( stateObj );
    }
  },
  initEventHandlers(){
    let self = this;
    window.addEventListener( 'click', function(event){
      self.initIdleResetTimeout();
    });
    document.querySelector('.attendees-list').addEventListener( 'scroll', this.initIdleResetTimeout.bind( this ) );
    //window.addEventListener( 'touchstart', this.initIdleResetTimeout.bind( this ) );
    this.canvas.addEventListener( 'click', this.handleCanvasClick.bind( this ) );
    window.addEventListener( 'resize', this.handleScreenResize.bind( this ) );
    this.attendeesList.addEventListener( 'click', event => {
      if(event.target.classList.contains('attendees-list-item')){
        var userId = parseInt(event.target.dataset.userId)
        for (var i=0; i < app.selectedAttendees.length; i++) {
          if (app.selectedAttendees[i].event_attendee_id === userId ) {
            // let nodes = app.activeState.parent.parent.childNodes.children.map( c => c.node );
            // nodes = nodes.filter(function( obj ) {
            //   console.log(obj.userId)
            //   return !obj.userId == app.selectedAttendees[i].event_attendee_id
            // });
            // app.unhighlightNodes(nodes);
            app.showSelectedAttendee( app.selectedAttendees[i] );
          }
        }
      }
    });
    this.resetButton.addEventListener( 'click', this.reset.bind( this ) );
    document.querySelector('.show-results').addEventListener( 'click', event => {
      this.hideSelectedAttendee();
      this.attendeesListContainer.classList.remove('hide');
      let evt = {type:'backToResults'}
      self.addEvent(evt);
    });
    document.querySelectorAll('.small-state-label').forEach( el => {
      el.addEventListener( 'click', event => {
        let stateName = el.dataset.state.split('-').join( ' ' );;
        let state = this.scene.getObjectByName( stateName ).children[0].children[0];
        this.highlightState( state );
      });
    });
    if( !this.skipControls && this.cameraControls ){
      this.cameraControls.addEventListener('change', event => {
        this.positionStateLabels();
        this.positionTextOverlays();
      });
    }
    document.addEventListener('contextmenu', event => event.preventDefault());
    document.getElementById('refresh').addEventListener('click', function(){
      window.location.reload();
    });
  },
  mouseClickToScene(clickEvent){
    let coordinates = ([clickEvent.clientX, clickEvent.clientY]);
    let [x,y] = coordinates;
    const sceneX = (( x / window.innerWidth ) * 2 - 1) * window.innerWidth / 2;
    const sceneY = (-1 * ( y / window.innerHeight ) * 2 + 1) * window.innerHeight / 2;
    return [sceneX, sceneY];
  },
  sceneToMouse( v ){
    v.project( this.camera );
    v.x = Math.round( (   v.x + 1 ) * this.canvas.offsetWidth  / 2 );
    v.y = Math.round( ( - v.y + 1 ) * this.canvas.offsetHeight / 2 );
    v.z = 0;
    return v;
  },
  handleScreenResize(){
    this.canvas.width = window.innerWidth;
    this.canvas.height = window.innerHeight;
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();
    if( this.camera.type === 'OrthographicCamera' ){
      this.camera.left = this.canvas.offsetWidth  * -1/2;
      this.camera.right = this.canvas.offsetWidth  * 1/2;
      this.camera.top = this.canvas.offsetHeight  * 1/2;
      this.camera.bottom = this.canvas.offsetHeight  * -1/2;
    }
  },
  getStateInfo (stateObj, allowOtherTweens=false ) {
    let sproutParent = stateObj.parent.parent;
    let stateName = sproutParent.name;
    let sproutInfo = this.stateInfo.what_is_office_state[stateName]
    let evt = { type: 'highlightState', state: stateName };
    this.addEvent(evt);
    this.activeState = stateObj;
    this.hideHelloMessage();
    this.showAttendeesList( stateName, sproutParent, sproutInfo );
    if( !allowOtherTweens && !sproutParent.isHighlighted){
      TWEEN.removeAll();
    }
    sproutParent.isHighlighed = true;
    // this.setStateAttendeeNodes( sproutParent );
    let largeScale = 1.5 * (this.resizeScales[stateName] || 1.0);
    let stateSize = this.getStateSize( sproutParent );
    let zValue = 10.0;
    let scaleValue = largeScale;
    let colorValue = this.highlightStateColor
    //if( this.ohioFun && stateObj.parent.parent.name === 'Ohio' ) colorValue = 1.0;    
    let scaleTween = this.createHighlightTween( stateObj, {scaleValue, zValue, colorValue}, true, 1000 );
    scaleTween.start();
    let sproutTween = this.createSproutTween( sproutParent.nodeDistance, 1.0, sproutParent);
    let attendees;
    if (sproutInfo) {
      attendees = sproutInfo.data.filter(function( obj ) {
        return !['Guest', 'Work Crew', 'Vendor', 'Admin', 'Read Only Admin', 'Board Member Invitee', 'LTA'].includes(obj.user_type)
      });
      sproutTween.delay( 200 ).start();
    }
    // let numberAttendees = (sproutInfo && sproutInfo.total) ? sproutInfo.total : 0
    let numberAttendees = (attendees && attendees.length) ? attendees.length : 0
    this.writeToTexture( this.numberNode.material.map, numberAttendees );
    if( this.resizeScales[stateName] ){
      let v = this.resizeScales[stateName] * 3/4;
      this.numberNode.scale.set(1/v,1/v,1);
    }else{
      this.numberNode.scale.set(1,1,1);
    }
    if( this.offsetCenters[stateName] ){
      this.numberNode.position.x = this.offsetCenters[stateName].x;
      this.numberNode.position.y = this.offsetCenters[stateName].y;
    }else{
      this.numberNode.position.x = 0;
      this.numberNode.position.y = 0;
    }
    stateObj.parent.parent.add( this.numberNode );
    this.numberNode.position.z +=1;
    this.smallStateLabelsContainer.classList.add('fade');
  },
  highlightState( stateObj, allowOtherTweens=false ){
    app.getStateInfo(stateObj, allowOtherTweens);
  },
  unhighlightState( stateObj, allowOtherTweens=false ){
    let evt = {type: 'unhighlightState', state: stateObj.parent.parent.name};
    this.addEvent(evt);
    let sproutParent = stateObj.parent.parent;
    let stateName = sproutParent.name;
    let sproutInfo = this.stateInfo.what_is_office_state[stateName]
    this.numberNode.parent.remove( this.numberNode );
    this.numberNode.position.z = 0;
    this.activeState = undefined;
    this.hideAttendeesList();
    this.clearAttendeesList();
    this.hideSelectedAttendee();
    if( !allowOtherTweens){
      TWEEN.removeAll();
    }
    sproutParent.isHighlighed = false;
    let scaleValue = this.resizeScales[stateObj.parent.parent.name] || 1.0;
    let zValue = 0;
    // let colorValue = "rgb(110, 110, 110)"
    let colorValue = "rgb(155, 155, 155)"
    //if( this.ohioFun && stateObj.parent.parent.name === 'Ohio' ) colorValue = 1.0;
    let scaleTween = this.createHighlightTween( stateObj, {scaleValue, zValue, colorValue}, false, 1000 );
    let sproutTween = this.createSproutTween( sproutParent.nodeDistance, 1.0, sproutParent, true);
    scaleTween.delay( 200 );
    scaleTween.start();
    if (sproutInfo) {
      sproutTween.start();
    }
    // let nodes = stateObj.parent.parent.childNodes.children.map( c => c.node );
    // this.unhighlightNodes(nodes);
    this.smallStateLabelsContainer.classList.remove('fade');
    if (allowOtherTweens) this.reset();
  },
  createHighlightTween( object, valuesObj, showChildren, duration ){
    let tween;
    let target = { 
      scaleValue: object.parent.parent.scale.x, 
      zValue: object.parent.parent.position.z,
      colorValue: object.material.color
    };

    let scaleTween = new TWEEN.Tween( target ).to( {
      scaleValue: valuesObj.scaleValue, zValue: valuesObj.zValue, colorValue: valuesObj.colorValue
    }, duration, TWEEN.Easing.Elastic.InOut );
    scaleTween.easing(this.easing);

    scaleTween.onUpdate( (v) => {
      let s = target.scaleValue;
      let z = target.zValue;
      // let color = target.colorValue;
      let color = valuesObj.colorValue;
      object.parent.parent.scale.set( s, s, 1 );
      object.parent.parent.position.z = z;
      this.setStateParentColor( object, color );
      //If this state is small, scale it up more when highlighted
      if( object.parent.parent.stateSize < 7 ){
        let p = Math.pow( 10 / object.parent.parent.stateSize, valuesObj.zValue > 0 ? v : 1.0 - v);
        //object.parent.scale.set( s * p, s * p, 1 );
        
        object.parent.parent.children.forEach(function(c){
          if(!c.skipScaleOnTween){
            c.scale.set( s * p, s * p, 1 ) 
          }
        });
      }
    });
    return scaleTween;
  },
  centerState( stateGroup ){
    let minX, minY, minZ, maxX, maxY, maxZ;
    let boundingBox = this.getStateGroupBoundingBox( stateGroup );
    minX = boundingBox.min.x;
    minY = boundingBox.min.y;
    minZ = boundingBox.min.z;
    maxX = boundingBox.max.x;
    maxY = boundingBox.max.y;
    maxZ = boundingBox.max.z;
    let center = new THREE.Vector3();
    center.x = (minX + maxX) / 2;
    center.y = (minY + maxY) / 2;
    center.z = (minZ + maxZ) / 2;
    stateGroup.boundingBox = boundingBox;
    stateGroup.stateSize = this.getStateSize( stateGroup );
    let childrenMesh = [];
    stateGroup.children.forEach( c => {
      childrenMesh.push( c.mesh, c.line );
      c.mesh.geometry.computeBoundingBox();
      c.line.geometry.computeBoundingBox();
    });
    childrenMesh.forEach( c => {
      c.position.sub( center );
    });
    stateGroup.position.add( center );
  },
  getStateGroupBoundingBox( stateGroup ){
    let mins, maxs;
    let childrenMesh = [];
    stateGroup.children.forEach( c => {
      childrenMesh.push( c.mesh, c.line );
      c.mesh.geometry.computeBoundingBox();
      c.line.geometry.computeBoundingBox();
    });
    mins = childrenMesh.map( c => c.geometry.boundingBox.min );
    maxs = childrenMesh.map( c => c.geometry.boundingBox.max );

    let minX = Math.min( ...mins.map( m => m.x ) );
    let minY = Math.min( ...mins.map( m => m.y ) );
    let minZ = Math.min( ...mins.map( m => m.z ) );
    let maxX = Math.max( ...maxs.map( m => m.x ) );
    let maxY = Math.max( ...maxs.map( m => m.y ) );
    let maxZ = Math.max( ...maxs.map( m => m.z ) );
    return { 
      min: new THREE.Vector3( minX, minY, minZ ),
      max: new THREE.Vector3( maxX, maxY, maxZ )
    }
  },
  setStateParentColor( stateMesh, color ){
    var newColor = new THREE.Color(color);
    stateMesh.parent.parent.children.forEach( c => {
      if( c.mesh ){
        c.mesh.material.color.setRGB( newColor.r, newColor.g, newColor.b );    
      }
    });
  },
  createStateChildNodes( stateObj ){
    let numNodes = 6;
    let childNodes = new THREE.Object3D();
    childNodes.skipScaleOnTween = true;
    childNodes.visible = false;
    stateObj.add( childNodes );
    stateObj.childNodes = childNodes;
    _.range(numNodes).forEach( i => {
      let angle = Math.PI * 2 * i / numNodes;
      let node = this.createChildNode( angle, stateObj );
      node.angleFromHome = angle;
      stateObj.childNodes.add( node );
      stateObj.childNodes.visible = false;
    });
    if( this.offsetCenters[stateObj.name] ){
      stateObj.childNodes.position.copy( this.offsetCenters[ stateObj.name ] );
    }
  },
  createChildNode( angle, parent ){
    let scale = this.resizeScales[parent.name] || 1;
    let d = this.getStateSize(parent);
    let l = d / 2 * 1.5 * scale;
    l = Math.max(Math.min( l, 12 ), 6 );
    //parent.nodeDistance = l;
    if( this.resizeScales[parent.name] ){
      parent.nodeDistance = 8 / this.resizeScales[parent.name]
    // }else if (parent.name == 'Washington' || parent.name == 'Montana') {
    //   parent.nodeDistance = 5
    }else if ( this.customNodeDistances[parent.name] ) {
      parent.nodeDistance = this.customNodeDistances[parent.name]
    }else{
      parent.nodeDistance = 8;
    }
    let v1 = new THREE.Vector3(0,0,-1);
    let v2 = new THREE.Vector3( l, 0, -1 );
    let axis = new THREE.Vector3(0,0,1);
    v2.applyAxisAngle( axis, angle );
    let tether = this.createTether( v1, v2 );
    let obj = new THREE.Object3D()
    obj.add( tether );
    let node = this.createNode( 1 / scale );
    node.position.copy( v2 );
    node.position.z += 2;
    obj.add( node );
    obj.tether = tether;
    obj.node = node;
    node.scale.set( this.smallScale, this.smallScale, this.smallScale );
    node.position.setLength( this.smallScale );
    tether.geometry.vertices[1].setLength( this.smallScale );
    return obj;
  },
  createTether( v1, v2 ){
    let geo = new THREE.Geometry();
    geo.vertices.push( v1, v2 );
    let material = new THREE.LineBasicMaterial({color: 0x000000});
    material.transparent = true;
    let tether = new THREE.Line( geo, material );
    tether.geometry.dynamic = true;
    return tether;
  },
  createNode( scale ){
    let size = this.nodeSize * scale;
    let geo = new THREE.PlaneBufferGeometry( size, size, 1 );
    let material = new THREE.ShaderMaterial({
      uniforms: {
        u_time: { type: 'f', value: 0.0 },
        tint: { type: 'f', value: 0.0 },
        imageTexture: { type: "t", value: undefined}
      },
      vertexShader: nodeImageVertexShader,
      fragmentShader: nodeImageFragmentShader,
      transparent: true
    });
    let mesh = new THREE.Mesh( geo, material );
    material.transparent = true;
    return mesh;
  },

  createSproutTween(distance, scale, sproutParent, reverse = false, controls = false ){
    if(!reverse || controls){
      this.controlsEnabled = false;
    }
    let start, end, duration;
    if(reverse){
      start = 1;
      end = this.smallScale;
      sproutParent.isSprouted = false;
    }else{
      start = this.smallScale
      end = 1;
      sproutParent.isSprouted = true;
      sproutParent.childNodes.visible = true;
    }
    let sproutTarget = { val: start };
    duration = reverse
        ? this.reverseSproutDuration
        : this.sproutDuration;
    let sproutTween = new TWEEN.Tween(sproutTarget).to(
      {val:end}, 
      duration,
    );
    if(reverse){
      sproutTween.easing( this.reverseEasing );
    }else{
      sproutTween.easing( this.easing );
    }
    let twoPI = Math.PI * 2;
    let parentScale = sproutParent.children[0].getWorldScale().x;

    sproutTween.onUpdate(() => {
      let scale = sproutTarget.val;
      let newValue = sproutTarget.val * distance;
      sproutParent.childNodes.children.forEach( (c, i) => {
        c.node.scale.set(scale, scale, scale);
        c.node.position.setLength( newValue );
        let tetherLength = Math.max( this.smallScale, newValue );
        c.tether.geometry.vertices[1].setLength( tetherLength );
        c.tether.geometry.verticesNeedUpdate = true;
      });
    });
    sproutTween.onComplete( () => {
      if(reverse){
        sproutParent.childNodes.children.forEach((c) =>{
          c.position.x = 0;
          c.position.y = 0;
        });
        sproutParent.childNodes.visible = false;
      }
    });
    return sproutTween;
  },
  search(valToFind, arrayToSearch, fieldOnObj){
    for (var i=0; i < arrayToSearch.length; i++) {
      if (arrayToSearch[i][fieldOnObj] === valToFind) {
        return arrayToSearch[i];
      }
    }
  },
  addUserPin( userObject ){
    if(!userObject.what_is_office_state) return;
    let sprite;
    // if (this.testStates) {
    //   userObject.what_is_office_state = this.testStates[0]
    //   this.testStates.splice( 0, 1 );
    // }
    let statePinExists = this.search(userObject.what_is_office_state.replace(/['"]+/g, '').trim(), this.pins, 'name');
    if(statePinExists){
      sprite = statePinExists;
    }else{
      let spriteMaterial = new THREE.SpriteMaterial({map: this.textures['red.png'], transparent:true, opacity: 1.0});
      sprite = new THREE.Sprite( spriteMaterial );
      sprite.scale.set( this.pinSize, this.pinSize, 1 );
      let pos = this.userLatLngToSceneMapCoordinates( userObject );
      if(!pos) return;
      pos.y += this.pinSize / 2;
      sprite.position.copy( pos );
      sprite.name = userObject.what_is_office_state.replace(/['"]+/g, '').trim()
      console.log("ADDING NEW PIN", sprite.name)
    }
    let userNames = sprite.userNames || new Array;
    let checkInName = userObject.first_name+' '+userObject.last_name
    let wasNameAdded, wasPinAdded;
    userNames.pushIfNotExist(checkInName, function(e) { 
      return wasNameAdded = e === checkInName;
    });
    if (!wasNameAdded){
      sprite.userNames = userNames;
      sprite.overlay = this.showTextOverlay( userObject, sprite );
    }

    this.pins.pushIfNotExist(sprite, function(e) { 
      return wasPinAdded = e.name === sprite.name;
    });
    if (!wasPinAdded) {
      this.scene.add( sprite );
    } else if (sprite.material.opacity < 1) {  
      console.log("tween back to showing", sprite.name)
      let tween = new TWEEN.Tween(sprite.material, {override: true}).to({
        opacity: 1
      }, 3000);
      sprite.tween = tween;
      tween.onUpdate(function(val){
        sprite.overlay.style.opacity = sprite.material.opacity;
      });
      tween.onComplete(function(){
        console.log('Tween Completed', sprite.name)
        sprite.overlay.classList.remove('hide');
        sprite.tween = undefined;
        // sprite.overlay.opacity = 1;
        // debugger
      });
      // this.scene.add( sprite );
      tween.start();
    } else {
      console.log('already exists', sprite)
    }
    return sprite;
  },
  hidePin( pin ){
    this.scene.remove( pin );
    if( pin.geometry ){
      pin.geometry.dispose();
    }
  },
  addPinTimeout( pin ){
    let self = this;
    pin.overlay.classList.remove('hide');
    pin.overlay.style.opacity = 0.8;
    pin.material.opacity = 0.8;
    if( pin.tween ){
      TWEEN.remove( pin.tween );
    }
    let tween = new TWEEN.Tween(pin.material, {override: true}).to({
      opacity: 0
    }, 5000);
    pin.tween = tween;
    tween.onUpdate(function(val){
      pin.overlay.style.opacity = pin.material.opacity;
    });
    tween.onComplete(function(){
      pin.overlay.classList.add('hide');
      // self.pins.splice( self.pins.indexOf(pin), 1 );
      // self.scene.remove( pin );
      pin.tween = undefined;
      //Remove text overlay from DOM
      if( pin && pin.overlay && pin.overlay.parentElement.children.length > 1 ){
        //pin.overlay.remove();
      }else{
        pin.overlay.classList.add('hide');
      }

      // let idx = self.pins.indexOf( pin );
      // self.pins.splice( idx, 1 );

    });
    tween.delay(5000).start();
  },
  addListItemRemoveTimeout(listItem){
    if(listItem.timeout) Meteor.clearTimeout( listItem.timeout );
    listItem.timeout = Meteor.setTimeout( () => {
      if(listItem.parentPin) listItem.parentPin.listItem = undefined;
      listItem.remove();
      if( $('.check-in-item:not(.hide)').length == 0 ){
        Meteor.call('clearTouchScreenScans', (err,response) => {
          if(err){
            console.log("Error clearing scans: ", err);
          }
        });        
      }
      listItem = undefined;
    }, 10050);
  },
  addUserCircleToMap( userObject){
    if( this.mode === 'day2' ) return;
    let pos = this.userLatLngToSceneMapCoordinates( userObject );
    if(!pos){
      return;
    }
    let geo = new THREE.SphereBufferGeometry(0.33, 4, 4);
    let material = new THREE.MeshBasicMaterial({color: 0xDD2A26});
    let mesh = new THREE.Mesh( geo, material );
    mesh.position.copy( pos );
    this.scene.add(mesh);
    this.checkInMeshes.push( mesh );
  },
  getStateNameFromPostal( postal ){
    return this.postalToState[postal];
  },
  showHelloMessage( attendee ){
    if( this.activeUser ) return;
    if (!attendee) return;
    app.currentAttendeeGreeting = attendee.event_attendee_id;
    var name = attendee.first_name + ' ' + attendee.last_name;
    this.scanName.innerText = name + '!';
    this.hasScanElement.classList.remove( 'hide' );
  },
  hideHelloMessage(){
    this.hasScanElement.classList.add( 'hide' );
    this.scanName.innerText = '';
  },
  createTextOverlays(){
    let numOverlays = 5;
    let overlay = this.textOverlayParent.children[0]
    this.textOverlays = [overlay];
    for( let i = 0; i < numOverlays - 1; i++ ){
      let clone = overlay.cloneNode(true);
      this.textOverlayParent.appendChild( clone );
      this.textOverlays.push( clone );
    }
  },
  createCheckInListItem( name ){
    let el = document.createElement('li');
    el.className = 'name';
    el.innerHTML = name;
    // el.dataset.userId = attendeeObj.event_attendee_id;
    // el.dataset.location = attendeeObj.what_is_office_state;
    return el;
  },
  showTextOverlay( userObject=false, sprite ){
    let names = sprite.userNames;
    let v = this.userLatLngToSceneMapCoordinates( userObject );
    if( !v ) return;
    v.y += this.pinSize / 2;
    let { first_name, last_name, classification } = userObject;
    let coordinates = this.sceneToMouse( v );
    /*
    let nextTextOverlay = this.textOverlays[(++this.nextOverlayIndex) % this.textOverlays.length];
    if( !nextTextOverlay.classList.contains('hide') ){
      let pin = this.pins.shift();
      this.hidePin( pin );
    }
    */

    //Create textoverlay and add to dom
    let overlay = this.textOverlayParent.children[0]
    let existingOverlay = document.querySelectorAll("[data-uuid='"+sprite.uuid+"']");
    let nextTextOverlay;
    if (existingOverlay.length) {
      nextTextOverlay = existingOverlay[0];
    } else {
      nextTextOverlay = overlay.cloneNode(true);
    }
    this.textOverlayParent.appendChild( nextTextOverlay );
    // Empty Existing List
    // nextTextOverlay.innerHTML = ''

    let checkInEls = names.map( n => {
      return app.createCheckInListItem( n );
    });
    nextTextOverlay.querySelector('.names').innerHTML = checkInEls.map( el => el.outerHTML ).join('');

    // if(userObject.classifications && typeof userObject.classifications === "object"){
    //   nextTextOverlay.querySelector('.classification').innerText = userObject.classifications.join(', ');
    // }
    nextTextOverlay.dataset.uuid = sprite.uuid;

    nextTextOverlay.style.top = -500 + 'px';
    nextTextOverlay.classList.remove('hide');
    //if(coordinates.x > 0.8 * window.innerWidth){
    if(false){
      let offset = nextTextOverlay.offsetWidth;
      nextTextOverlay.style.left = (coordinates.x - offset - 10) + 'px';
    }else{
      // nextTextOverlay.style.left = (coordinates.x + 10) + 'px';
      nextTextOverlay.style.left = (coordinates.x + 20) + 'px';
    }
    nextTextOverlay.style.top = (coordinates.y - 20) + 'px';

    return nextTextOverlay;
  },
  createOverlaySprite(v, userObject){
    let textCanvas = this.nodeTextureFallbacks[0];
    let texture = this.nodeTextures[0];
    texture.image = textCanvas;
    let { firstName, lastName, classification } = userObject;
    this.writeNameToTexture( texture, firstName + ' ' + lastName, classification );
    let spriteMaterial = new THREE.SpriteMaterial({ map: texture, color: 0x0000ff});
    let sprite = new THREE.Sprite( spriteMaterial );
    sprite.scale.set( 12,4,1 );
    v.z = 3;
    v.x += 2;
    v.y -= 1;
    sprite.position.copy( v );
    sprite.position.z = 3;
    
    this.scene.add( sprite );
  },
  hideTextOverlay(){
    this.textOverlay.classList.add('hide');
    this.textOverlay.style.top = '-1000px';
    this.textOverlay.style.left = '-1000px';
    this.textOverlay.querySelector('.name').innerText = '';
    this.textOverlay.querySelector('.classification').innerText = '';
  },
  loadTextures(){
    this.textures = {}
    let textureFiles = [
      'red.png'
    ];
    textureFiles.forEach( filename => {
      this.textureLoader.load( filename, map => {
        this.textures[filename] = map;
      });
    });
  },
  resizeStates( ){
    Object.keys(this.resizeScales).forEach( k => {
      let state = this.scene.getObjectByName( k );
      let v = this.resizeScales[k];
      state.scale.set( v, v, 1.0 );
      let p = this.offsetPositions[k];
      state.position.x += p.x;
      state.position.y += p.y;
    });
  },
  initTextContent(){
    this.initTime();
    this.initTemperature();
    this.initSessionText()
  },
  initSessionText() {
    this.sessionElement.innerText = this.session;
  },
  initTime(){
    this.timeElement.innerText = this.getTime();
    this.timeInterval = setInterval( () => {
      this.timeElement.innerText = this.getTime();      
    }, 30 * 1000);
  },
  getTime(){
    return moment().format('h:mmA');
  },
  initTemperature(){
      this.fillInTemperature();
    this.temperatureInterval = setInterval( () => {
      this.fillInTemperature();
    }, 10 * 60 * 1000)
  },
  fillInTemperature(){
    let url = "https://api.wunderground.com/api/5a1c28c97f165ac6/conditions/q/NV/Las_Vegas.json";
    let req = new Request( url );
    fetch( req ).then( response => {
      return response.json();
    }).catch( reject => {
      console.log('Error: ', reject );
    }).then( jsonObj => {
      let temp = jsonObj.current_observation.temp_f;
      this.temperatureElement.innerHTML = Math.round(temp) + '&deg;F';
    });
  },
  listenForPasserby() {
    // var client = mqtt.connect('mqtts://10.0.1.22');
    // var client = mqtt.connect('tcp://10.0.1.22');
    // var client = mqtt.connect('tls://10.0.1.22');
    // var client = mqtt.connect('ws://10.0.1.22');
    // var client = mqtt.connect('wss://10.0.1.22');

    // (‘mqtt://’, ‘mqtts://’, ‘tcp://’, ‘tls://’, ‘ws://’, ‘wss://’)

    // client.on('connect', function () {
    //   client.subscribe('132', function (err) {
    //     if (!err) {
    //       client.publish('132', 'Hello mqtt')
    //     }
    //   })
    // })
    
    // client.on('message', function (topic, message) {
    //   // message is Buffer
    //   console.log(message.toString())
    //   client.end()
    // })

    // app.mqtt = mqtt.connect('mqtt:/10.0.1.22');
 
    // app.mqtt.on('connect', function () {
    //   app.mqtt.subscribe('132', function (err) {
    //     if (!err) {
    //       app.mqtt.publish('132', 'Hello mqtt')
    //     }
    //   })
    // })
    
    // app.mqtt.on('message', function (topic, message) {
    //   // message is Buffer
    //   console.log(message.toString())
    //   // app.mqtt.end()
    // })
  },
  runDay1Mode(){
    this.mode = 'day1';
    let showContent = document.querySelectorAll('.day-1');
    let hideContent = document.querySelectorAll('.day-2');
    showContent.forEach( c => c.classList.remove('hide') );
    hideContent.forEach( c => c.classList.add('hide') );
  },
  runDay2Mode(){
    this.mode = 'day2';
    let showContent = document.querySelectorAll('.day-2');
    let hideContent = document.querySelectorAll('.day-1');
    showContent.forEach( c => c.classList.remove('hide') );
    hideContent.forEach( c => c.classList.add('hide') );
  },
  mockCheckinStream(){
    this.handleCheckin( this.getMockUser());
    this.mockCheckinInterval = setInterval( () => {
      this.handleCheckin(this.getMockUser());
    }, 3 * 1000);
  },
  mockScanStream(){
    let user = this.getMockUser();
    let antenna = this.antenna;
    this.handleNewScan( {antenna}, user );
    this.mockScanInterval = setInterval( () => {
      let user = this.getMockUser();
      this.handleNewScan( {antenna}, user );
    }, 5 * 1000);
  },
  getMockUser(){
    let postals = Object.keys(postalToState);
    let postal = postals[Math.floor(Math.random() * postals.length)];
    let users = WallUsers.find({state: postal}).fetch();
    let randomUser = users[Math.floor(Math.random() * users.length)];
    if( !randomUser ) return this.getMockUser();
    return randomUser;
  },
  handleCheckin( user ){
    if( this.mode === 'day1'){
      this.addUserCircleToMap( user );
    }
  },
  lookupUserAndShowHelloMessage( scan ) {
    let codes = scan.codes
    $.ajax({
      // url: 'http://10.0.0.6/api/v1/admin/events/'+app.eventId+'/tag_lookup?code='+code+'&select=id,first_name,last_name,city,state,region,area,what_is_office_state,chairmans_circle,presidents_club,presidents_lifetime,presidents_club_new_ring,sales_leader_club,presidents_club_new_lifetime,user_avatar_file_name',
      // url: 'https://api2.eventfinity.co/api/v1/admin/events/'+app.eventId+'/tag_lookup?code='+code+'&multiple=1&select=id,first_name,last_name,city,state,region,area,what_is_office_state,chairmans_circle,presidents_club,presidents_lifetime,presidents_club_new_ring,sales_leader_club,presidents_club_new_lifetime,user_avatar_file_name',
      // url: 'https://api2.eventfinity.co/api/v1/admin/events/'+app.eventId+'/tag_lookup?code='+codes+'&multiple=1&select=id,first_name,last_name,city,state,region,area,what_is_office_state,chairmans_circle,presidents_club,presidents_lifetime,presidents_club_new_ring,sales_leader_club,presidents_club_new_lifetime,user_avatar_file_name',
      url: 'http://10.0.0.6/api/v1/admin/events/'+app.eventId+'/tag_lookup?code='+codes+'&multiple=1&select=id,first_name,last_name,city,state,region,area,what_is_office_state,chairmans_circle,presidents_club,presidents_lifetime,presidents_club_new_ring,sales_leader_club,presidents_club_new_lifetime,user_avatar_file_name',
      type: 'POST',
      dataType: 'json',
      success: function(response) {
        if ( response.attendees.length > 0 ){
          var scannedAttendees = response.attendees
          var attendee = _.sample(scannedAttendees)
          var remaining = _.without(scannedAttendees, attendee);
          if( app.mode == 'day1' ){
            app.queue.unshift(attendee)
            remaining.map( a => {
              app.addUserToQueue(a)
            });
          } else {
            console.log('Scanned Attendee: ', attendee)
            app.showHelloMessage( attendee );
          }
          let evt = { type: 'greetAttendee', attendeeId: attendee.event_attendee_id }
          app.addEvent(evt)
        } else {
          console.log('NO ATTENDEE FOUND - Success')
        }
      },
      error: function() { 
        console.log('NO ATTENDEE FOUND - Fail')
      },
      // beforeSend: app.setHeader
    });
    app.initNoScanTimeout();
  },
  handleNewScan( scan, mock ){
    if( this.antenna !== scan.antenna.toString()){
      return;
    }
    this.initNoScanTimeout();
  },
  showAttendeesList( stateName, sproutParent, sproutInfo ){
    this.activeStateName.innerText = stateName;
    this.getAttendeesList( stateName, sproutParent, sproutInfo );
  },
  hideAttendeesList( stateName ){
    this.attendeesListContainer.classList.add('hide');
  },
  getAttendeesList( stateName, sproutParent, sproutInfo ){
    // if (sproutInfo) {}
    app.attendeesListContainer.classList.remove('hide');
    app.activeContainer.classList.remove( 'hide' );
    if(!sproutInfo) return {};
    let attendees = sproutInfo.data
    app.clearAttendeesList
    attendees = attendees.filter(function( obj ) {
      return !['Guest', 'Work Crew', 'Vendor', 'Admin', 'Read Only Admin', 'Board Member Invitee', 'LTA'].includes(obj.user_type)
    });
    let attendeesEls = attendees.map( a => {
      return app.createAttendeeListItem( a );
    });
    app.selectedAttendees = attendees;
    app.attendeesList.innerHTML = attendeesEls.map( el => el.outerHTML ).join('');
    app.setStateAttendeeNodes( sproutParent, sproutInfo );
  },
  getAttendeesCount( stateName ){
    var total = 0
    for (var i=0; i < app.stateTotals.length; i++) {
      if (app.stateTotals[i].what_is_office_state === stateName) {
        total = app.stateTotals[i].total;
      }
    }
    return total
  },
  clearAttendeesList(){
    this.attendeesList.innerHTML = '';
  },
  createAttendeeListItem( attendeeObj ){
    if (['Guest', 'Work Crew', 'Vendor', 'Admin', 'Read Only Admin', 'Board Member Invitee', 'LTA'].includes(attendeeObj.user_type) ) return;
    let el = document.createElement('div');
    el.className = 'attendees-list-item';
    el.innerHTML = attendeeObj.first_name + ' ' + attendeeObj.last_name;
    el.dataset.userId = attendeeObj.event_attendee_id;
    return el;
  },
  showSelectedAttendee( user){
    let evt = { type: 'showAttendee', attendeeId: user.event_attendee_id }
    this.addEvent(evt)
    //let user = this.getMockUser();
    // let user = WallUsers.findOne( userId );
    this.hideAttendeesList();
    app.selectedAttendee.set( user );
    this.selectedAttendeeEl.classList.remove('hide');
  },
  hideSelectedAttendee(){
    this.selectedAttendeeEl.classList.add('hide');
    app.selectedAttendee.set('');    
  },
  ohioFun( ){
    if (this.ohio == '1') {
      //let ohio = this.scene.getObjectByName('Ohio').children[2].children[0];
      //this.ohio = ohio;
      let ohioGroup = this.scene.getObjectByName('Ohio');
      let texture = this.mikeTexture.clone();
      texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
      texture.repeat.set( .11, .11 );
      texture.offset.set( 0.3, 0.15 );
      ohioGroup.children.forEach( p => {
        let ohio = p.children[0];
        ohio.material = new THREE.MeshBasicMaterial({map: texture});
        ohio.material.color.set( 0xffffff );
        ohio.material.map = texture;
        ohio.material.needsUpdate = true;
        ohio.material.map.needsUpdate = true;
      });
      // this.ohioFun = true;
    }
  },
  handleActiveUser(){
    this.activeUser = true;
    this.hideHelloMessage();
    this.clearTouchIcons();
  },
  handleUserLeft(){
    this.activeUser = false;
    this.activeContainer.classList.add( 'hide' );
    this.hideAttendeesList();
    this.clearAttendeesList();
    this.hideHelloMessage();
    if( this.activeState ){
      this.unhighlightState( this.activeState );
    }    
    this.runTouchIconAnimation(true);
  },
  setStateAttendeeNodes( stateGroup, sproutInfo, mock = false ){
    stateGroup.childNodes.visible = true;
    let childNodes = stateGroup.childNodes.children;
    let attendees = sproutInfo.data.filter(function( obj ) {
      return !['Guest', 'Work Crew', 'Vendor', 'Admin', 'Read Only Admin', 'Board Member Invitee', 'LTA'].includes(obj.user_type)
    });
    let nodeAttendees = this.getNRandomValuesFromArray( attendees, childNodes.length)
    if(nodeAttendees.length < childNodes.length && !mock){
      childNodes.forEach( (c,i) => {
        let angle = Math.PI * 2 * i / nodeAttendees.length + Math.PI;
        let axis = new THREE.Vector3(0,0,1);
        let pos = new THREE.Vector3(1,0,0).applyAxisAngle( axis, angle).setLength(this.smallScale);
        c.node.position.copy( pos );
        c.tether.geometry.vertices[1] = pos.clone();
      });
    }
    childNodes.forEach( (c, i) => {
      let texture = this.nodeTextures[i];
      let fallback = this.nodeTextureFallbacks[i];
      this.setNodeAttendee( c.node, nodeAttendees[i], texture, fallback, mock );
    });
  },
  setNodeAttendee( node, attendeeObj, texture, fallbackCanvas, mock ){
    //This sets the node's imageTexture uniform to the given texture,
    //and sets the image of that texture to the attendeeObj's headshot image or a fallback
    if( attendeeObj === undefined ){
      if( mock ){
        attendeeObj = this.getMockUser();
      }else{
        node.parent.visible = false;
        return;
      }
    }else{
      node.parent.visible = true;
    }
    if(attendeeObj.user_avatar_file_name && attendeeObj.event_attendee_id){
      texture.image = fallbackCanvas;
      let firstName = attendeeObj.first_name;
      let lastName = attendeeObj.last_name;
      this.writeNameToTexture( texture, firstName, lastName );
      node.material.uniforms.imageTexture.value = texture;
      node.material.uniforms.imageTexture.value.needsUpdate = true;
      let filepath = "https://eventfinity-production-assets.s3.amazonaws.com/user_avatars/"+attendeeObj.event_attendee_id+"/original/"+attendeeObj.user_avatar_file_name
      // let filepath = attendeeObj.user_avatar_url
      this.textureLoader.crossOrigin = 'anonymous'
      this.textureLoader.load(filepath, texture => {
        if(node.material.uniforms.imageTexture.value){
          node.material.uniforms.imageTexture.value.dispose();
        }
        // texture.image.width = 256
        // texture.image.height = 256
        node.material.uniforms.imageTexture.value = texture
        // node.material.uniforms.imageTexture.value.width = 256
        // node.material.uniforms.imageTexture.value.height = 256
      });
    }else{
      texture.image = fallbackCanvas;
      let firstName = attendeeObj.first_name;
      let lastName = attendeeObj.last_name;
      this.writeNameToTexture( texture, firstName, lastName );
      node.material.uniforms.imageTexture.value = texture;
      node.material.uniforms.imageTexture.value.needsUpdate = true;
    }
    node.material.needsUpdate = true;
    node.userId = attendeeObj.event_attendee_id;
  },
  getNRandomValuesFromArray(srcArr, n) {
    // making copy to do not affect original srcArray
    let arr = srcArr.slice();
    // let arr =  _.sample(srcArr, n);
    // console.log(arr.length)
    let resultArr = [];
    // while srcArray isn't empty AND we didn't enough random elements
    while (arr.length && resultArr.length < n) {
      // remove one element from random position and add this element to the result array
      resultArr = resultArr.concat( // merge arrays
        arr.splice( // extract one random element
          Math.floor(Math.random() * arr.length),
          1
        )
      );
    }
    return resultArr;
},
  selectRandomStateAttendees( sproutInfo, n ){
    var arr = sproutInfo.data
    var result = new Array(n),
        len = arr.length,
        taken = new Array(len);
    if (n > len)
        throw new RangeError("getRandom: more elements taken than available");
    while (n--) {
        var x = Math.floor(Math.random() * len);
        result[n] = arr[x in taken ? taken[x] : x];
        taken[x] = --len in taken ? taken[len] : len;
    }
    return result;
  },
  positionAKHIContainer(){
    let ak = this.scene.getObjectByName( 'Alaska' );
    let pos = ak.getWorldPosition();
    let screenPos = this.sceneToMouse( pos );
    let y = screenPos.y - 104;
    let x = screenPos.x - 70;
    this.akhiContainer.style.top = y + 'px';
    this.akhiContainer.style.left = x + 'px';
  },
  createAKHIContainer(){
    let ak = this.scene.getObjectByName( 'Alaska' );
    let akbbmin = ak.boundingBox.min.clone();
    let akbbmax = ak.boundingBox.max.clone();
    let v1 = new THREE.Vector3( -100, 50, 1 );
    let v2 = new THREE.Vector3( -90, 50, 1 );
    let v3 = new THREE.Vector3( -86, 46, 1 );    
    let lineGeo = new THREE.Geometry();
    lineGeo.vertices.push( v1, v2, v3 );
    let material = new THREE.LineBasicMaterial({color: 0x000000});
    let line = new THREE.Line( lineGeo, material );
    this.scene.akContainer = line;
    this.scene.add( line );
  },
  createStateNumberTexture( debug ){
    if( !this.textCanvas ){
      this.textCanvas = document.createElement( 'canvas' );
      this.textCanvas.className = "hidden-text-canvas hide";
      document.body.appendChild( this.textCanvas );
    }
    if( debug ) this.textCanvas.classList.remove('hide');
    this.textCanvas.width = 256;
    this.textCanvas.height = 256;
    this.textCanvasContext = this.textCanvas.getContext( '2d' );
    this.textTexture = new THREE.Texture( this.textCanvas );    
  },
  writeToTexture(texture, text, skipUpdate){
    let canvas = texture.image;
    let ctx = this.textCanvasContext;
    // ctx.fillStyle = 'rgba(0,0,0,0)';
    ctx.fillStyle = 'rgba(255,255,255,0)';
    ctx.clearRect(0,0, canvas.width, canvas.height);
    ctx.fillRect(0,0, canvas.width, canvas.height);
    if(text.toString().length < 3){
      ctx.font = 'Bold ' + this.numberFontSize + 'px SF Display';
    }else{
      ctx.font = 'Bold ' + (this.numberFontSize * 0.8) + 'px SF Display';
    }
    ctx.fillStyle = 'rgb(255,255,255,1)';
    ctx.strokeStyle = 'white';
    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';
    ctx.strokeText( text, canvas.width/2, canvas.height /2);    
    ctx.fillText( text, canvas.width/2, canvas.height /2);  
    texture.needsUpdate = !skipUpdate;
  },
  writeNameToTexture(texture, firstName, lastName){
    let canvas = texture.image;
    let ctx = canvas.getContext('2d' );
    ctx.clearRect(0,0, canvas.width, canvas.height);
    ctx.fillStyle = 'rgba(255,0,0,1)';
    ctx.fillRect(0,0, canvas.width, canvas.height);
    ctx.font = 'Bold ' + (this.numberFontSize/4) + 'px Arial';
    ctx.fillStyle = 'rgba(255,255,255,1)';
    ctx.strokeStyle = 'white';
    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';
    ctx.fillText( firstName, canvas.width/2, (canvas.height /2 -25) );
    ctx.fillText( lastName, canvas.width/2, (canvas.height /2 + 25) );
  },
  createNumberNode( ){
    let geo = new THREE.PlaneBufferGeometry( 5, 5, 1 );
    let material = new THREE.MeshBasicMaterial({color: 0xFFFFFF, transparent: true, map: this.textTexture});
    let mesh = new THREE.Mesh( geo, material );
    this.numberNode = mesh;
    this.numberNode.name = 'numberNode';
    this.numberNode.skipScaleOnTween = true;
    //this.numberNode.visible = false;
  },
  getStateSize( stateObj){
    let bb = stateObj.boundingBox;
    return bb.min.distanceTo( bb.max );
  },
  getAvgStateSize(){
    let ds = this.statesGroup.children.map( sg => this.getStateSize( sg ) );
    return ds.reduce( (a,b) => a+b, 0 ) / ds.length;
  },
  createSmallStateLabelReferences(){
    this.smallStateLabels = {};
    let els = new Array(...document.querySelectorAll('.small-state-label'));
    els.forEach( el => {
      let name = el.dataset.state.split('-').join(' ');
      this.smallStateLabels[name] = el;
    });
  },
  positionStateLabels( ){
    Object.keys(this.smallStateLabels).forEach( k => {
      let el = this.smallStateLabels[k];
      let stateGroup = this.scene.getObjectByName(k);
      let screenPos = this.sceneToMouse( stateGroup.getWorldPosition() );
      let offsetX = screenPos.x + 10;
      let offsetY = screenPos.y - 5;
      offsetX += +el.dataset.offsetX || 0;
      offsetY += +el.dataset.offsetY || 0;
      el.style.top = offsetY + 'px';
      el.style.left = offsetX + 'px';
    });
  },
  positionTextOverlays(){
    this.pins.forEach( p => {
      if(!p.overlay.classList.contains('hide')){
        let userObject = WallUsers.findOne(p.userId);
        let v = this.userLatLngToSceneMapCoordinates( userObject );
        v.y += this.pinSize / 2;
        v = this.sceneToMouse( v );
        p.overlay.style.top = (v.y - 20) + 'px';
        p.overlay.style.left = (v.x + 10) + 'px';
      }
    });
  },
  reset(){
    if( this.mode === 'day1' ){
      return;
    }else{
      if( this.activeState ){
        this.unhighlightState( this.activeState );
      }
      Meteor.call('clearTouchScreenScans', (err,response) => {
        if(err){
          console.log("Error clearing scans: ", err);
        }
      });
      this.handleUserLeft();
    }
  },
  initNoScanTimeout(){
    if(this.mode !== 'day2') return;
    if( this.noScanTimeout ){
      clearTimeout( this.noScanTimeout );
    }
    this.noScanTimeout = setTimeout( () => {
      this.hideHelloMessage();
    }, this.noScanTimeoutDuration );
  },
  initIdleResetTimeout(){
    if(this.mode === 'day1') return;
    if( this.idleTimeout ){
      clearTimeout( this.idleTimeout );
    }
    this.idleTimeout = setTimeout( () => {
      this.reset();
    }, this.idleTimeoutDuration );
  },
  createNodeTextures(){
    let numNodes = 6;
    this.nodeTextures = [];
    this.nodeTextureFallbacks = [];
    _.range( numNodes ).forEach( i => {
      let canvas = document.createElement('canvas');
      canvas.width = 256;
      canvas.height = 256;
      this.nodeTextureFallbacks.push( canvas );
      let texture = new THREE.Texture();
      this.nodeTextures.push( texture );
    });
    
  },
  initCollectionObservers(){
    let self = this;
    Scans.find().observe({
      added( scan ){
        if (scan.readerId == app.readerId) {
          if (app.lastScan) {
            var overTenSeconds = Date.now() - app.lastScan > 10000
            console.log("LAST SCAN", app.lastScan)
            console.log('Over 10?', overTenSeconds)
            if (overTenSeconds) {
              app.lookupUserAndShowHelloMessage(scan)
              app.lastScan = Date.now()
            }
          } else {
            app.lastScan = Date.now()
            app.lookupUserAndShowHelloMessage(scan)
          }
          // app.initNoScanTimeout();
        }
      },
      removed( scan ){
        self.reset();
      }
    });
    ConfigValues.find().observe({
      added( cv ){
        if( cv.name === 'touchIdleTimeoutDuration' ){
          if( this.idleTimeout ){
            clearTimeout( this.idleTimeout );
          }
          self.idleTimeoutDuration = cv.value * 1000;
          if( self.activeUser ){
            self.initIdleResetTimeout();
          }
        }
      },
      changed(newCV, oldCV){
        if( newCV.name === 'touchIdleTimeoutDuration' ){
          if( self.idleTimeout ){
            clearTimeout( self.idleTimeout );
          }
          self.idleTimeoutDuration = newCV.value * 1000;
          if( self.activeUser ){
            self.initIdleResetTimeout();
          }
        }
      }
    });
  },
  clearScans(){
    Meteor.call( 'clearTouchScreenScans', (err, response) => {
      if(err) alert(err);
    });
  },
  clearTouchIcons(){
    TWEEN.removeAll();
    document.querySelector('.touch-icons').classList.add('hide');
    /*
    this.touchIcons.forEach( (icon, i) => { 
      icon.classList.add(
    */
  },
  positionTouchIcons(){
    this.touchIcons.forEach( (icon, i) => {
      Meteor.setTimeout( () => {
        icon.classList.remove('hide');
        this.positionTouchIcon( icon );
      }, (i||0.0001) * 2 * 1000);
    });
  },
  positionTouchIcon(icon){
    icon.style.top = Math.floor(Math.random() * window.innerHeight) + 'px';
    icon.style.left = Math.floor(Math.random() * window.innerWidth) + 'px';
  },
  positionCenterTouchIcon(){
    let icon = document.querySelector('.center-touch-icon')
    icon.style.top = '40%';
    icon.style.left = '50%';
    // icon.style.top = Math.floor(Math.random() * window.innerHeight) + 'px';
    // icon.style.left = Math.floor(Math.random() * window.innerWidth) + 'px';
    icon.classList.remove('hide');
  },
  runTouchIconAnimation(repeat = false){
    if(this.touchInterval){
      clearInterval( this.touchInterval );
    }
    // this.positionTouchIcons();
    this.positionCenterTouchIcon();
    document.querySelector('.touch-icons').classList.remove('hide');
    if(!repeat){
      this.touchInterval = setInterval( () => {
        this.positionTouchIcons();
      }, 8000 );
    }
  },
  getFunFactsFromUser( userObject ){
    let funFactFields = [
      'favoriteOlympicSport', 'favoriteMovie', 'hiddenTalent', 'dreamVacation',
      'favorite_food', 'when_young_wanted_to_be_a'
    ];
    return funFactFields.filter( f => userObject[f] )
      .map( f => { 
        let key = f[0].toUpperCase() + f.split(/(?=[A-Z])/).join(" ").slice(1);
        if (key == "When_young_wanted_to_be_a") {
          key = "When Young, Wanted to Be A"
          // key = "When I was young I wanted to be a"
        } else if (key == "Favorite_food") {
          key = "Favorite Food"
        }
        //key = key.split('_').map( m => m[0].toUpperCase() + m.slice(1)).join(' ');
        key = key.split('_').map( m => m[0] + m.slice(1)).join(' ');
        let value = userObject[f];
        value = value[0].toUpperCase() + value.slice(1);
        return {key, value}
      });
  },
  addEvent( event ){
    let category, label;
    category = this.mode == 'day1' ? "Check Ins" : "Touch Screen"
    label = "state" in event ? 'state' : 'attendeeId'
    event.createdAt = new Date();
    Meteor.call( 'addEvent', event, function(err,response){
      if(err) console.log(err);
    });
    gtag('event', event.type, {
      'event_category': category,
      'event_label': event[label]
    });
  },
  getUrlVars() {
    var vars = {};
    window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
  },
  getUrlParam(parameter, defaultValue){
    var urlParam = defaultValue;
    if(window.location.href.indexOf(parameter) > -1){
      urlParam = this.getUrlVars()[parameter];
    }
    return urlParam;
  },
  checkUrlParams() {
    this.session = this.getUrlParam('session', '1');
    this.eventId = this.getUrlParam('event-id', '631');
    this.ohio = this.getUrlParam('ohio', '0');
    this.readerId = this.getUrlParam('reader', '1');
  },
  getCheckIns() {
    $.ajax({
      // url: 'https://api2.eventfinity.co/api/v1/admin/analytics/events/'+app.eventId+'/checkin_data?amount=10&group_by=what_is_office_state&select=id,first_name,last_name,city,state,region,area,what_is_office_state,chairmans_circle,presidents_club,presiden ts_lifetime,presidents_club_new_ring,sales_leader_club,presidents_club_new_lifetime,user_type&where=session_checkin:'+app.session,
      // url: 'https://api2.eventfinity.co/api/v1/admin/analytics/events/'+app.eventId+'/checkin_data?amount=10&group_by=what_is_office_state&select=id,first_name,last_name,city,state,region,area,what_is_office_state,chairmans_circle,presidents_club,presidents_lifetime,presidents_club_new_ring,sales_leader_club,presidents_club_new_lifetime,user_type&orWhere=waldo_show:1,session_checkin:'+app.session,
      // url: 'https://api2.eventfinity.co/api/v1/admin/analytics/events/'+app.eventId+'/checkin_data?amount=10&group_by=what_is_office_state&select=id,first_name,last_name,city,state,region,area,what_is_office_state,chairmans_circle,presidents_club,presidents_lifetime,presidents_club_new_ring,sales_leader_club,presidents_club_new_lifetime,user_type,session_checkin,waldo_show,user_avatar_file_name&orWhere=session_checkin:'+app.session+',waldo_show:1',
      url: 'https://api2.eventfinity.co/api/v1/admin/analytics/events/'+app.eventId+'/checkin_data?amount=10&group_by=what_is_office_state&select=id,first_name,last_name,city,state,region,area,what_is_office_state,chairmans_circle,presidents_club,presidents_lifetime,presidents_club_new_ring,sales_leader_club,presidents_club_new_lifetime,user_type,session_checkin,waldo_show,user_avatar_file_name&orWhere=session_checkin:'+app.session+',waldo_show:1&notWhere=user_type:Guest,user_type:Work%20Crew,user_type:Vendor,user_type:Admin,user_type:Read%20Only%20Admin,user_type:Board%20Member%20Invitee,user_type:LTA',
      type: 'GET',
      dataType: 'json',
      success: function(response) {
        var users = response.data
        app.stateTotals = response.what_is_office_state_count
        var total_checkins = response.overall_checkins_count
        app.updateTotalCheckins(total_checkins);
        $.map( users.reverse(), function( val, i ) {
          if (!['Guest', 'Work Crew', 'Vendor', 'Admin', 'Read Only Admin', 'Board Member Invitee', 'LTA'].includes(val.user_type) ) {
            app.addUserToQueue(val)
          }
        })
      },
      beforeSend: app.setHeader
    });
  },
  setHeader(xhr) {
    xhr.setRequestHeader('X-AUTH-TOKEN', '75ae483d9de99972dd0292096763731c64770638438ddeb8c8b48d1d21164c1a');
  },
  updateTotalCheckins(total_checkins) {
    $('.check-in-count').text(total_checkins);
  },
  checkIfUserInPastQueue(user) {
    var i;
    for (i = 0; i < this.pastQueue.length; i++) {
      if (this.pastQueue[i].event_attendee_id === user.event_attendee_id) {
        return true;
      }
    }
    return false;
  },
  addUserToQueue(user) {
    if (this.checkIfUserInPastQueue(user)) return;
    this.queue.pushIfNotExist(user, function(e) { 
      return  e.event_attendee_id === user.event_attendee_id;
    });
  },
  addUserToPastQueue(user) {
    this.pastQueue.pushIfNotExist(user, function(e) { 
      return  e.event_attendee_id === user.event_attendee_id;
    });
  },
  addUserFromQueue() {
    if(this.queue.length == 0) return;
    let listItems = $('.check-in-item:not(.original)')
    if (listItems.length < this.maxListItems) {
      let user = this.queue[0]
      this.addUserToList(user)
      this.queue = this.queue.filter(function( obj ) {
        return obj.event_attendee_id !== user.event_attendee_id;
      });
      this.addUserToPastQueue(user)
      // Keep the pastQueue small so we dont end up with thousands in memory
      // Ensure that we wont have any overlap where we show the same people twice
      if (this.pastQueue.length > 20) this.pastQueue.pop();
    } else if (listItems.length == this.maxListItems) {
      let lastItem = listItems.last()
      if (lastItem[0].parentPin) {
        console.log('Pin Names:', lastItem[0].parentPin.userNames)
        var userName = lastItem[0].dataset.userName;
        var i = lastItem[0].parentPin.userNames.indexOf(userName);
        if (i > -1) {
          lastItem[0].parentPin.userNames.splice(i,1);
        }
        // app.showTextOverlay( false, lastItem[0].parentPin )
        // console.log('lastItem[0].parentPin.userNames.length == 0', lastItem[0].parentPin.userNames.length == 0)
        if (lastItem[0].parentPin.userNames.length == 0) {
          console.log("userNames = 0 ", lastItem[0].parentPin.userNames.length == 0)
          app.addPinTimeout( lastItem[0].parentPin )
        }
      }
      if(lastItem[0].parentPin) lastItem[0].parentPin.listItem = undefined;
      lastItem.remove();
      let user = this.queue[0]
      this.addUserToList(user)
      this.queue = this.queue.filter(function( obj ) {
        return obj.event_attendee_id !== user.event_attendee_id;
      });
      this.addUserToPastQueue(user)
    }
  },
  addUserToList(user) {
    let pin = this.addUserPin( user );    
    let maxListItems = this.maxListItems;
    let listItemExists = $('.check-in-item.' + user.event_attendee_id);
    if(listItemExists.length){
      // $(listItemExists[0]).removeClass('flipInX')
      // $(listItemExists[0]).addClass('slideInDown')
      // app.addListItemRemoveTimeout( listItemExists[0] );
      return
    }
    let listItems = document.querySelectorAll('.check-in-item');
    // let clone = listItems[0].cloneNode(true);
    let clone = document.querySelector('.original').cloneNode(true);
    let filepath;
    if (user.user_avatar_file_name) {
      filepath = "https://eventfinity-production-assets.s3.amazonaws.com/user_avatars/"+user.event_attendee_id+"/original/"+user.user_avatar_file_name
    } else {
      filepath = "https://place-hold.it/350x350/c72119/ffffff.png&text="+user.first_name[0]+user.last_name[0]+"&fontsize=130"
    }
    clone.querySelector('.attendee-img').src = filepath;
    clone.querySelector('.attendee-img').classList.remove('hidden');
    clone.querySelector('.check-in-name').innerText = user.first_name + ' ' + user.last_name;
    clone.dataset.userId = user.event_attendee_id;
    clone.dataset.location = user.what_is_office_state;
    clone.dataset.userName = user.first_name + ' ' + user.last_name;

    // if (user.chairmans_circle == "Yes") {
    //   clone.querySelector('.chairmans-circle').classList.remove('hide')
    // }

    // if (user.sales_leader_club == "Yes") {
    //   clone.querySelector('.leaders-club').classList.remove('hide')
    // }

    // if  (user.sales_leader_club !== 'Yes' && user.chairmans_circle !== "Yes") {
    //   if (user.presidents_club == "Yes" || user.presidents_lifetime == "Yes" || user.presidents_club_new_ring == "Yes") {
    //     clone.querySelector('.presidents-club').classList.remove('hide')
    //   }
    // }

    if (user.what_is_office_state) {
      clone.querySelector('.check-in-origin').innerText = user.what_is_office_state.replace(/['"]+/g, '').trim();
    }else{
      clone.querySelector('.check-in-origin').innerText = '';
    }
    clone.classList.add(user.event_attendee_id);
    let parent = document.querySelector('.check-ins-list-container');
    if(listItems.length > maxListItems && listItems[1]){
      listItems[1].remove();
    }

    clone.classList.remove('hide', 'original');
    $(clone).prependTo(parent);
    if( pin ){
      clone.parentPin = pin
    }
  },
  waitForImageToLoad(imageElement){
    return new Promise(resolve=>{imageElement.onload = resolve})
  },
}
app.selectedAttendee = new ReactiveVar({});
Template.main.helpers({
  selectedAttendee(){
    let user = app.selectedAttendee ? app.selectedAttendee.get() : {};
    if(!user) return {};
    if (user.first_name && user.last_name) {
      let initials = user.first_name[0]+user.last_name[0];
      $('.attendee-initials').text(initials)
      $('#selected-attendee-img').addClass('hide')
      $('.initials-container').removeClass('hide');
    }
    user.name = user.first_name + ' ' + user.last_name;
    user.location = user.what_is_office_state ? user.what_is_office_state.replace(/['"]+/g, '').trim() : '';
    user.funFacts = app.getFunFactsFromUser( user );
    user.hasFunFacts = user.funFacts.length > 0;
    // user.hasImage = false;
    if(user.user_avatar_file_name && user.event_attendee_id){
      let filepath = "https://eventfinity-production-assets.s3.amazonaws.com/user_avatars/"+user.event_attendee_id+"/original/"+user.user_avatar_file_name
      user.imgSrc = filepath;
      let selectedAttendeeImage = document.getElementById('selected-attendee-img');
      app.waitForImageToLoad(selectedAttendeeImage).then(()=>{
        $('.initials-container').addClass('hide')
        $('#selected-attendee-img').removeClass('hide')
      });
      // user.hasImage = true;
      // user.imgSrc = '/photos/' + user.userAvatarUrl.split('original/')[1];
    }
    // if(user.classifications && typeof user.classifications === "object"){
    //   user.classifications = user.classifications.join(', ');
    // }
    if (user.year_started){
      user.years = 2018 - (+ user.year_started);
    }
    return user;
  },
  checkinCount(){
    return WallUsers.find({"epcs.0.epc": {$exists: true}}).count();
  },
});

Template.main.onCreated( function(){
  let antenna = window.location.search.split('a=')[1];
  app.antenna = antenna ? antenna : "70";
  Meteor.subscribe('touchScreenScans', app.antenna);
  Meteor.subscribe('wallUsers');
  Meteor.subscribe('configValues');
  Meteor.subscribe('scans');
});

Template.main.rendered = () => {
  // See if this is needed. As long as GA event is sent, dont need to save scans
  Meteor.call('clearScans', (err,response) => {
    if(err) console.log(err);
  });
  app.initScene();
  app.loadMap();
  app.animate();
  app.initEventHandlers();
  app.checkUrlParams();
  app.loadTextures();
  app.createTextOverlays();
  app.initTextContent();
  if( window.location.search.indexOf('?1') !== -1 ){
    app.runDay1Mode();
    app.getCheckIns();
    setInterval( () => { app.getCheckIns() }, 10000);
    setInterval( () => { app.addUserFromQueue() }, 1700);
  }else{
    app.runDay2Mode();   
    app.runTouchIconAnimation(true);
  }
  app.createStateNumberTexture();
  app.createNumberNode();
  app.createNodeTextures();
  app.createSmallStateLabelReferences();
  app.initIdleResetTimeout();
  app.textureLoader.load( 'mike.jpg', map => {
    app.mikeTexture = map
  });

  setTimeout( () => {
    app.positionAKHIContainer();
    app.ohioFun();
    app.positionStateLabels();
    app.initCollectionObservers();
  }, 3000 );
}

window.app = app;
window.THREE = THREE;
window.TWEEN = TWEEN;
window.Scans = Scans;
window.TouchScreenScans = TouchScreenScans;
window.WallUsers = WallUsers;
window.ConfigValues = ConfigValues;
