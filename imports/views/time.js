'use strict';

import './time.html';
import './time.css';

import { Template } from 'meteor/templating'
import { ReactiveVar } from 'meteor/reactive-var'
import { Meteor } from 'meteor/meteor';
import { ConfigValues } from '/imports/api/configValues';

Template.time.onCreated(() => {
  Meteor.subscribe('configValues');
});

Template.time.helpers({
  idleTimeoutDuration(){
    let cv = ConfigValues.findOne({'name': 'touchIdleTimeoutDuration'});
    return cv ? cv.value : 30;
  },
  idleTimeoutSeconds(){
    let cv = ConfigValues.findOne({'name': 'touchIdleTimeoutDuration'});
    return cv ? cv.value % 60 : 0;
  },
  idleTimeoutMinutes(){
    let cv = ConfigValues.findOne({'name': 'touchIdleTimeoutDuration'});
    return cv ? Math.floor(cv.value / 60) : 0;
  }
})
Template.time.events({
  'click .increase':(e,t) => {
    Meteor.call('changeTimeoutDuration', 1, (err,res) => {
      if( err ) console.log(err);
    });
  },
  'click .decrease':(e,t) => {
    Meteor.call('changeTimeoutDuration', -1, (err,res) => {
      if( err ) console.log(err);
    });
  }
})
