import csv, sys, urllib, requests, json, os

def get_photos( directory ):
    #filenames = get_photo_filenames( csv_filename )
    filenames = get_photo_filenames_from_paperless();
    # data = [[user['email'].replace('.', '%'), user['user_avatar_url']] for user in filenames]
    data = [[user['event_attendee_id'], user['user_avatar_url']] for user in filenames]
    save_photos( data, directory );

def get_photo_filenames( filename ):
    names = []
    with open(filename, 'rb') as csvfile:
        csvreader = csv.reader(csvfile)
        for line in csvreader:
            names.append((line[3], line[5]))
    return names

def get_photo_filenames_from_paperless():
    # url = "https://butterfly.paperless.events/api/v1/admin/attendees/search?event_id=19&asc=false&paginate=false"
    # headers = {'X-AUTH-TOKEN': '2cf9c01ae51f2a28950954552de5f5040cbc51beccc3522948200a877745be4f'}
    # r = requests.get(url, headers=headers)
    dir_path = os.path.dirname(os.path.realpath(__file__))
    # print dir_path
    with open(dir_path+'/lib/stateTest.json') as f:
        data = json.load(f)
    # data = r.json()['data']
    us = data["what_is_office_state"]["Texas"]["data"]
    users_with_images = [u for u in us if u['user_avatar_url'] is not None and u['user_avatar_url'] is not 'null']
    return users_with_images
    


def save_photos( filenames, directory ):
    print 'Getting ' + str(len(filenames)) + " files."
    for i, f in enumerate(filenames):
        if i > 0:
            url = f[1]
            #filename = filename.replace(" ", "_")
            filename = url.split('original/')[1]
            '''
            if f.find(".jpg") == -1 and f.find(".png") == -1:
                print f
                #f += ".png"
            '''
            filepath = directory + "/" + filename
            urllib.urlretrieve( url, filepath );
            print "File #" + str(i) + " (" + filename + ") retrieved"


if __name__ == "__main__":
    directory = sys.argv[1]
    #csv_filename = sys.argv[1]
    #base_url = sys.argv[2]
    get_photos(directory)
