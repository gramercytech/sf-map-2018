import os
import stat
import subprocess
import sys
import imghdr
from PIL import Image
import time


def process_images(force_create_thumbs=False):
    Image.MAX_IMAGE_PIXELS = 1000000000
    #convert_mov_to_mp4("s3/2017/12/11/19/54/58/df1563a2-795d-440e-bee0-d68d9ee30c92/02 Heaven Hill Fire.mov");
    #path = "../_serverAssets"
    path = os.path.dirname(os.path.realpath(__file__)) + '/../_serverAssets'
    for root, subdirs, files in os.walk(path):
        for file in os.listdir(root):
            filepath = os.path.join(root, file)
            if os.path.isdir(filepath):
                pass
            elif imghdr.what(filepath) and not file.endswith(".thumb") and not file.endswith(".med"):
                create_thumb(filepath, force_create_thumbs)
            elif not imghdr.what(filepath) and file.endswith('.jpg'):
                #Force it on jpgs that aren't being returned as valid
                create_thumb(filepath, force_create_thumbs)

def create_thumb(filepath, force_create_thumb=False):
    size = (400,400)
    new_filepath = filepath + ".thumb"
    new_filepath_medium = filepath + ".med"
    try:
        update_thumbs = force_create_thumb or file_has_been_updated(filepath)
        create_medium = not os.path.exists(new_filepath_medium) or update_thumbs
        create_thumb = not os.path.exists(new_filepath) or update_thumbs
        if create_medium or create_thumb:
            original = Image.open(filepath)
            print('Processing ' + filepath)
            
        if create_medium:
            print('Creating medium sized version')
            im = original.copy()
            im.thumbnail((600, im.size[1] * 600 / im.size[0]))
            im.save(new_filepath_medium, original.format)
            
        if create_thumb:
            print('Creating thumbnail')
            im = original.copy()
            im.thumbnail(size)
            im.save(new_filepath, original.format)

    except:
        print("cannot create thumbnail for", new_filepath, sys.exc_info())

def file_has_been_updated(filepath):
    age = (time.time() - os.stat(filepath)[stat.ST_MTIME]) / 60 / 60
    return age < 23        

if __name__ == "__main__":
    process_images()
