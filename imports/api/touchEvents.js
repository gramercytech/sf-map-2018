import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

export const TouchEvents = new Mongo.Collection('touchEvents');

Meteor.methods({
  addEvent( evt ){
    let newEvent = TouchEvents.insert( evt );
    return newEvent;
  }
});

if( Meteor.isServer ){
  Meteor.publish( 'touchEvents', function(){ return TouchEvents.find() } );
}
