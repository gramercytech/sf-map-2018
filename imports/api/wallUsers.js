import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

export const WallUsers = new Mongo.Collection('wallUsers');

Meteor.methods({

});

if( Meteor.isServer ){
  Meteor.publish( 'wallUsers', function(){ return WallUsers.find() } );
}
