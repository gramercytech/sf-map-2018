import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

//In memory collection for scans specifically for the touch screen
export const TouchScreenScans = new Mongo.Collection('touchScreenScans', { connection: null });
// export const ConfigValues = new Mongo.Collection('configValues');

Meteor.methods({
  addTouchScreenScan( scan ){
    // scan.epc = scan.address;
    // scan.userID = scan.epc;
    scan.createdAt = Date.now();
    TouchScreenScans.insert( scan );
  },
  clearTouchScreenScans( ){
    return TouchScreenScans.remove({});
  }
});

if( Meteor.isServer ){
  Meteor.publish( 'touchScreenScans', function(antenna){ 
    if(antenna){
      return TouchScreenScans.find({antenna}) 
    }else{
      return TouchScreenScans.find() 
    }
  } );
}
