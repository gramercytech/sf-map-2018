import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

export const Scans = new Mongo.Collection('scans');

Meteor.methods({
  addScan( scan ){
    Scans.insert( scan );
  },
  clearScans( scan ){
    Scans.remove({});
  }
});

if( Meteor.isServer ){
  Meteor.publish( 'scans', function(){ return Scans.find() } );
}
