import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

export const ConfigValues = new Mongo.Collection('configValues');

Meteor.methods({
  changeTimeoutDuration(delta){
    let current = ConfigValues.findOne({name: 'touchIdleTimeoutDuration'});
    let currentValue = current ? +current.value : 30;
    if( (currentValue + delta) < 0){
      currentValue = 0;
      delta = 0;
    }
    ConfigValues.upsert({name: 'touchIdleTimeoutDuration'}, {$set: {value: currentValue + delta}});
  }
});

if( Meteor.isServer ){
  Meteor.publish( 'configValues', function(){ return ConfigValues.find() } );
}
