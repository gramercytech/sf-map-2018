'use strict';

import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';

//Load templates
import '../../views/main.js';
import '../../views/time.js';


FlowRouter.route("/", {
  name: "main",
  action(params){
    BlazeLayout.render("main", {viewName: "main"});
  }
});

FlowRouter.route("/t", {
  name: "time",
  action(params){
    BlazeLayout.render("time", {viewName: "time"});
  }
});
