exports.tetherVertexShader = `
  varying float lineU;
  attribute float vertexNumber;
  uniform float flag1;
  uniform float flag2;
  uniform float tweenValue;
  uniform float opacity;
  void main() {
    lineU = vertexNumber;
    gl_Position = projectionMatrix * modelViewMatrix * vec4(position.x, position.y, position.z, 1.0);
  }
`

exports.tetherFragmentShader = `
uniform float flag1;
uniform float flag2;
varying float lineU;
uniform float tweenValue;
uniform float opacity;
void main(){

  if(lineU > flag1 && lineU < flag2){
    gl_FragColor = vec4( 1.0, 1.0, 1.0, opacity );
  }else{
    discard;
  }
}
`

exports.tetherFragmentShader2 = `
uniform float flag1;
uniform float flag2;
varying float lineU;
uniform float tweenValue;
uniform float opacity;
void main(){
  if(lineU > flag1 && lineU < flag2){
    gl_FragColor = vec4( 1.0, 1.0, 1.0, opacity );
  }else{
    discard;
  }
}
`

exports.nodeImageVertexShader = `
uniform sampler2D imageTexture;
varying vec2 vUv;
  uniform float opacity;
  uniform float u_time;
  uniform float tint;
  void main() {
    vec3 newposition;
    vUv = uv;
    if (tint > 0.0) {
      //newposition = position * (1.0 + sin(u_time * 3.0) * 0.2 );
      newposition = position * (1.0 + sin(u_time * 3.0) * 0.15 );
    } else {
      newposition = position;
    }
    gl_Position = projectionMatrix * modelViewMatrix * vec4(newposition.x, newposition.y, newposition.z, 1.0);
  }
`

exports.nodeImageFragmentShader = `
uniform float u_time;
//uniform float tweenValue;
//uniform float opacity;
uniform float tint;
uniform sampler2D imageTexture;
varying vec2 vUv;
void main(){
  vec4 imageColor  = texture2D(imageTexture, vec2(vUv.x, vUv.y));
  //vec4 imageColor  = vec4( 1,1,1,1);
  //float colVal = sin( u_time );
  //vec4 imageColor  = vec4( vUv.x + colVal, vUv.y, 0.0, 1.0 );
  /*
  imageColor.a *= opacity;
  imageColor.r *= tint;
  imageColor.g *= tint;
  imageColor.b *= tint;
  */
  float d = distance( vUv.xy, vec2( 0.5, 0.5 ) );
  if( d > 0.5 ){
    discard;
  }else if( d > 0.49){
    float val = smoothstep( 0.5, 0.49, d );
    gl_FragColor = vec4( 0.0, 0.0, 0.0, val );
  }else if( d > 0.45){
    gl_FragColor = vec4( 0.0+tint, 0.0+tint, 0.0+tint, 1.0 );
  }else{
    gl_FragColor = imageColor;
  }
}
`
