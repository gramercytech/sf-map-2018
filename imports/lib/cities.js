export const cities = {
  "Alabama": {
    "city": "Montgomery",
    "state": "AL",
    "lat": 32.3792233,
    "lng": -86.3077368
  },
  "Alaska": {
    "city": "Juneau",
    "state": "AK",
    "lat": 58.3019444,
    "lng": -134.4197221
  },
  "Arizona": {
    "city": "Phoenix",
    "state": "AZ",
    "lat": 33.4483771,
    "lng": -112.0740373
  },
  "Arkansas": {
    "city": "Little Rock",
    "state": "AR",
    "lat": 34.7464809,
    "lng": -92.2895948
  },
  "California": {
    "city": "Sacramento",
    "state": "CA",
    "lat": 38.5815719,
    "lng": -121.4943996
  },
  "Colorado": {
    "city": "Denver",
    "state": "CO",
    "lat": 39.7392358,
    "lng": -104.990251
  },
  "Connecticut": {
    "city": "Hartford",
    "state": "CT",
    "lat": 41.7658043,
    "lng": -72.6733723
  },
  "Delaware": {
    "city": "Dover",
    "state": "DE",
    "lat": 39.158168,
    "lng": -75.5243682
  },
  // "Florida": {
  //   "city": "Tallahassee",
  //   "state": "FL",
  //   "lat": 30.4382559,
  //   "lng": -84.2807329
  // },
  "Florida": {
    "city": "Orlando",
    "state": "FL",
    "lat": 28.5383355,
    "lng": -81.3792365
  },
  // "Florida": {
  //   "city": "Jacksonville",
  //   "state": "FL",
  //   "lat": 30.3321838,
  //   "lng": 81.655651
  // },
  "Georgia": {
    "city": "Atlanta",
    "state": "GA",
    "lat": 33.7489954,
    "lng": -84.3879824
  },
  "Hawaii": {
    "city": "Honolulu",
    "state": "HI",
    "lat": 21.3069444,
    "lng": -157.8583333
  },
  "Idaho": {
    "city": "Boise",
    "state": "ID",
    "lat": 43.6187102,
    "lng": -116.2146068
  },
  "Illinois": {
    "city": "Springfield",
    "state": "IL",
    "lat": 39.7817213,
    "lng": -89.6501481
  },
  "Indiana": {
    "city": "Indianapolis",
    "state": "IN",
    "lat": 39.768403,
    "lng": -86.158068
  },
  "Iowa": {
    "city": "Des Moines",
    "state": "IA",
    "lat": 41.5868353,
    "lng": -93.6249593
  },
  "Kansas": {
    "city": "Topeka",
    "state": "KS",
    "lat": 39.0473451,
    "lng": -95.6751576
  },
  "Kentucky": {
    "city": "Frankfort",
    "state": "KY",
    "lat": 38.2009055,
    "lng": -84.8732835
  },
  "Louisiana": {
    "city": "Baton Rouge",
    "state": "LA",
    "lat": 30.4514677,
    "lng": -91.1871466
  },
  "Maine": {
    "city": "Augusta",
    "state": "ME",
    "lat": 44.3106241,
    "lng": -69.7794897
  },
  "Maryland": {
    "city": "Annapolis",
    "state": "MD",
    "lat": 38.9784453,
    "lng": -76.4921829
  },
  "Massachusetts": {
    "city": "Boston",
    "state": "MA",
    "lat": 42.3600825,
    "lng": -71.0588801
  },
  "Michigan": {
    "city": "Lansing",
    "state": "MI",
    "lat": 42.732535,
    "lng": -84.5555347
  },
  "Minnesota": {
    "city": "Saint-Paul",
    "state": "MN",
    "lat": 44.9537029,
    "lng": -93.0899578
  },
  "Mississippi": {
    "city": "Jackson",
    "state": "MS",
    "lat": 32.2987573,
    "lng": -90.1848103
  },
  "Missouri": {
    "city": "Jefferson City",
    "state": "MO",
    "lat": 38.5767017,
    "lng": -92.1735164
  },
  "Montana": {
    "city": "Helena",
    "state": "MT",
    "lat": 46.5891452,
    "lng": -112.0391057
  },
  "Nebraska": {
    "city": "Lincoln",
    "state": "NE",
    "lat": 40.8257625,
    "lng": -96.6851982
  },
  "Nevada": {
    "city": "Carson City",
    "state": "NV",
    "lat": 39.1637984,
    "lng": -119.7674034
  },
  "New Hampshire": {
    "city": "Concord",
    "state": "NH",
    "lat": 43.2081366,
    "lng": -71.5375718
  },
  "New Jersey": {
    "city": "Trenton",
    "state": "NJ",
    "lat": 40.2205824,
    "lng": -74.759717
  },
  "New Mexico": {
    "city": "Santa Fe",
    "state": "NM",
    "lat": 35.6869752,
    "lng": -105.937799
  },
  "New York": {
    "city": "Albany",
    "state": "NY",
    "lat": 42.6525793,
    "lng": -73.7562317
  },
  "North Carolina": {
    "city": "Raleigh",
    "state": "NC",
    "lat": 35.7795897,
    "lng": -78.6381787
  },
  "North Dakota": {
    "city": "Bismarck",
    "state": "ND",
    "lat": 46.8083268,
    "lng": -100.7837392
  },
  "Ohio": {
    "city": "Columbus",
    "state": "OH",
    "lat": 39.9611755,
    "lng": -82.9987942
  },
  "Oklahoma": {
    "city": "Oklahoma City",
    "state": "OK",
    "lat": 35.4675602,
    "lng": -97.5164276
  },
  "Oregon": {
    "city": "Salem",
    "state": "OR",
    "lat": 44.9428975,
    "lng": -123.0350963
  },
  "Pennsylvania": {
    "city": "Harrisburg",
    "state": "PA",
    "lat": 40.2731911,
    "lng": -76.8867008
  },
  "Rhode Island": {
    "city": "Providence",
    "state": "RI",
    "lat": 41.8239891,
    "lng": -71.4128343
  },
  "South Carolina": {
    "city": "Columbia",
    "state": "SC",
    "lat": 34.0007104,
    "lng": -81.0348144
  },
  "South Dakota": {
    "city": "Pierre",
    "state": "SD",
    "lat": 44.3667876,
    "lng": -100.3537522
  },
  "Tennessee": {
    "city": "Nashville",
    "state": "TN",
    "lat": 36.1626638,
    "lng": -86.7816016
  },
  "Texas": {
    "city": "Austin",
    "state": "TX",
    "lat": 30.267153,
    "lng": -97.7430608
  },
  "Utah": {
    "city": "Salt Lake City",
    "state": "UT",
    "lat": 40.7607793,
    "lng": -111.8910474
  },
  "Vermont": {
    "city": "Montpelier",
    "state": "VT",
    "lat": 44.2600593,
    "lng": -72.5753869
  },
  "Virginia": {
    "city": "Richmond",
    "state": "VA",
    "lat": 37.5407246,
    "lng": -77.4360481
  },
  "Washington": {
    "city": "Olympia",
    "state": "WA",
    "lat": 47.0378741,
    "lng": -122.9006951
  },
  "West Virginia": {
    "city": "Charleston",
    "state": "WV",
    "lat": 38.3498195,
    "lng": -81.6326234
  },
  "Wisconsin": {
    "city": "Madison",
    "state": "WI",
    "lat": 43.0730517,
    "lng": -89.4012302
  },
  "Wyoming": {
    "city": "Cheyenne",
    "state": "WY",
    "lat": 41.1399814,
    "lng": -104.8202462
  }
}